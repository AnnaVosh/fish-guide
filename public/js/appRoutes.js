

angular.module('appRoutes', []).config(
    ['$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {

    $routeProvider

    // home page
        .when('/', {
            templateUrl : 'views/home.html',
            controller  : 'MainController'
        })

// (Использование режима HTML5 если возможно.)
    $locationProvider.html5Mode(true);

}]);