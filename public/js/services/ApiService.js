angular.module('ApiService', []).factory('ApiService', ['$http', function ($http) {

    return {

        getAllFish: function () {
            return $http.get('/api/get_all_fish');
        },

        addFish: function (data) {
            return $http.post('/api/add_fish', data);
        },

        editFish: function (data) {
            return $http.put('/api/edit_fish', data);
        },

        deleteFish: function (data) {
            return $http.post('/api/delete_fish', data);
        },
    }

}]);