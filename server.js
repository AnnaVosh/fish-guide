// ======================= modules ==========================
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var methodOverride = require('method-override');

// ==================== methods =============================
function removeAllData(model) {
    model.find({}).remove().exec();
}

function printAllData(model) {
    model.find(function (err, data) {
        if (err) return console.error(err);
        for (var i in data) {
            console.log(data[i])
        }
    });
}

function fillModel(model, data) {
    removeAllData(model);
    for (var i in data) {
        new model(data[i]).save(
            function (err) {
                if (err) return console.error(err);
            }
        )
    }
}

// ==================== configuration =======================

// путь к конфигурациям базы
var db = require('./config/db');

// устанавливается порт приложения
var port = process.env.PORT || 7000;

// connect to our mongoDB database
console.log(db.url);
mongoose.connect(db.url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('Подключение к базе успешно установлено.');
    // fillModel(Triplets, tripletInitData);
    // fillModel(Rules, RuleInitData)
});

// настраеваем bodyParser
// (при поступлении запроса на сервер, первым делом его подхватывает
// боди-парсер, он вытаскивает всю целевую информацию, посланную клиентом
// и помещает ее в req.body. Например, если какашка посылала fname:"luna" --->
// информация будет помещена в req.body.fname)
//
// настраиваем его на парсинг трех типов запросов:
// 1) application/json ---> обычный жсон
app.use(bodyParser.json());
// 2) application/vnd.api+json as json ---> я хз
app.use(bodyParser.json({type: 'application/vnd.api+json'}));
// 3) application/x-www-form-urlencoded ---> когда параметры передаются в строке запроса
app.use(bodyParser.urlencoded({extended: true}));


// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override'));


// устанавливаем статичную папку, в которой нахадится клиетт
app.use(express.static(__dirname + '/public'));


// ====================== устанавливаются маршруты ============================
require('./app/routes')(app); // configure our routes


// ===================== start app ==========================
// startup our app at http://localhost:7000
app.listen(port, function () {
    console.log('Magic happens on port ' + port);
});


// устанавливаем экспорт, чтобы другие файлы при доступе к server.js
// через require получали обратно app
exports = module.exports = app;
