// grab the mongoose module
var mongoose = require('mongoose');
var FishSchema = mongoose.Schema({
    title: String,
    place: String,
    time: String,
    lure: String,
    bait: String,
    method: String,
    description: String
});

module.exports = mongoose.model('Fish', FishSchema);
