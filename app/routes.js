'use strict';
var Fish = require('./models/fish');



var jsonfile = require('jsonfile');

module.exports = function (app) {

    // ========================= серверные роуты ==================================
    app.post('/api/add_fish', function (req, res) {
        new Fish(req.body).save(function (err) {
            if (err) {
                console.log('Error when create new fish: ' + err);
            }
            res.send(200);
        });
    });

    app.get('/api/get_all_fish', function (req, res) {
        Fish.find({}, function (err, fish) {
            if (err) {
                res.send(err);
            }
            res.send(fish);
        });
    });

    app.put('/api/edit_fish', function (req, res) {
        Fish.findOne({_id:req.body._id}, function(err,doc) {
            doc.title = req.body.title;
            doc.place = req.body.place;
            doc.time = req.body.time;
            doc.lure = req.body.lure;
            doc.bait = req.body.bait;
            doc.method = req.body.method;
            doc.description = req.body.description;
            doc.save(
                function(err) {
                    if (err) return console.error(err);
                    res.send(200);
                }
            )
        });
    });

    app.post('/api/delete_fish', function (req, res) {
        Fish.find({_id:req.body._id}).remove(() => {
            res.send(200);
        })
    });


    app.get('*', function (req, res) {
        res.sendfile('./public/index.html'); // load our public/index.html file
    });

}
;